<link href="{{ asset('css/photojam.css') }}" rel="stylesheet">
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if ($photos[0]->face_name === "unknown")
                    <div class="panel-heading">Unknown Faces (Tag each face to improve the predictions)</div>
                @else
                    <div class="panel-heading">{{ $photos[0]->face_name }}</div>
                @endif
                <div class="panel-body">
                  @foreach ($photos as $photo)
                  <div class="main-images">
                    <a href="/home/photos/face/{{ $photo->file_name }}"><img src="{{ url($photo->path) }}"/></a>
                  </div>
                  @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
