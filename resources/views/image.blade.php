<link href="{{ asset('css/photojam.css') }}" rel="stylesheet">
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if ($image->face_name === "unknown")
                    <div class="panel-heading">Unknown Face</div>
                @else
                    <div class="panel-heading">
                        <a href="/home/photos/{{ $image->face_name }}">
                        {{ $image->face_name }}
                        </a>
                    </div>
                @endif
                <div class="panel-body">
                  <div class="main-images image-view">
                    <img src="{{ url($image->path) }}"/>
                  </div>
                  <form method="POST" action="/home/photos/{{ $image->file_name }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        @if ($image->face_name === "unknown")
                            <label>Who is this?</label>
                        @else
                            <label>Not {{ $image->face_name }}? Tag the correct person.</label>
                        @endif

                        <input type="text" class="form-control" id="name" name="name">
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>

                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
