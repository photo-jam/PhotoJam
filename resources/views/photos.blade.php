
<link href="{{ asset('css/photojam.css') }}" rel="stylesheet">
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your Photos</div>
                <div class="panel-body">
                    @foreach ($photos as $photo)
                    <div class="main-images">
                        <a href="/home/photos/{{ $photo->face_name }}"><img src="{{ url($photo->path) }}"/></a>
                        <span class="image-text">{{ $photo->face_name }}</span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
