@extends('layouts.app')

@section('content')
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to PhotoJam</div>
                    <a class="button" href="{{ url('/home/photos') }}">
                        See Your Photos
                    </a>
                <div class="panel-body">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
