<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;
use DB;
use Storage;

class PhotoController extends Controller
{
  public function __construct()
  {
     $this->middleware('auth');
  }

  public function index()
  {
    $all_photos = Photo::all();
    $unique_photos = array();
    $seen_faces = array();

    foreach ($all_photos as &$photo){
      if (in_array($photo->face_name, $seen_faces)){
        //
      }
      else {
        $s3_path = "Camera Uploads/" . $photo->file_name;
        $s3_url = Storage::disk('s3')->temporaryUrl($s3_path, now()->addMinutes(10));
        $photo->path = $s3_url;
        $seen_faces[] = $photo->face_name;
        $unique_photos[] = $photo;
      }
    }
    return view('photos', ['photos' => $unique_photos]);
  }

  public function show_face($face_name)
  {
    $photos = Photo::where('face_name', '=', $face_name)->get();

    foreach ($photos as $photo) {
      $s3_path = "Camera Uploads/" . $photo->file_name;
      $s3_url = Storage::disk('s3')->temporaryUrl($s3_path, now()->addMinutes(10));
      $photo->path = $s3_url;
    }

    return view('person', ['photos' => $photos]);
  }

  public function show_image($file_name)
  {
    $image = Photo::where('file_name', '=', $file_name)->get();
    $image = $image[0];
    $s3_path = "Camera Uploads/" . $image->file_name;
    $s3_url = Storage::disk('s3')->temporaryUrl($s3_path, now()->addMinutes(10));
    $image->path = $s3_url;

    return view('image', ['image' => $image]);
  }

  public function update($file_name)
  {
    $photos = Photo::all();
    DB::table('photos')->where('file_name', '=', $file_name)->update(['face_name' => request('name')]);

    foreach ($photos as $photo){
      $s3_path = "Camera Uploads/" . $photo->file_name;
      $s3_url = Storage::disk('s3')->temporaryUrl($s3_path, now()->addMinutes(10));
      $photo->path = $s3_url;
    }
    return view('photos', ['photos' => $photos]);
  }
}
