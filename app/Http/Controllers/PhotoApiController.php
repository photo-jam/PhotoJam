<?php

namespace App\Http\Controllers;
use App\Photo;
use Storage;
use DB;

use Illuminate\Http\Request;

class PhotoApiController extends Controller
{
  public function __construct()
  {
     $this->middleware('auth:api');
  }

  public function index()
  {
    $all_photos = Photo::select('face_name', 'path', 'file_name')->get();
    $unique_photos = array();
    $seen_faces = array();

    foreach ($all_photos as &$photo){
      if (in_array($photo->face_name, $seen_faces)){
        //
      }
      else {
        $s3_path = "Camera Uploads/" . $photo->file_name;
        $s3_url = Storage::disk('s3')->temporaryUrl($s3_path, now()->addMinutes(10));
        $photo->path = $s3_url;
        $seen_faces[] = $photo->face_name;
        $unique_photos[] = $photo;
      }
    }
    return $unique_photos;
  }

  public function show_face($face_name)
  {
    $photos = Photo::where('face_name', '=', $face_name)->select('face_name', 'path', 'file_name')->get();
    foreach ($photos as &$photo){
      $s3_path = "Camera Uploads/" . $photo->file_name;
      $s3_url = Storage::disk('s3')->temporaryUrl($s3_path, now()->addMinutes(10));
      $photo->path = $s3_url;
      }
    return $photos;
  }

  public function show_image($file_name)
  {
    $image = Photo::where('file_name', '=', $file_name)->select('face_name', 'path', 'file_name')->get();
    $image = $image[0];
    $s3_path = "Camera Uploads/" . $image->file_name;
    $s3_url = Storage::disk('s3')->temporaryUrl($s3_path, now()->addMinutes(10));
    $image->path = $s3_url;

    return $image;
  }

  public function update(Request $request, $file_name)
  {
    DB::table('photos')->where('file_name', '=', $file_name)->update(['face_name' => request('name')]);
    return "post success";
  }

  public function photo_upload(Request $request)
  {
    $decoded_image = base64_decode(request('encoded_image'));
    Storage::disk('s3')->put(request('name'), $decoded_image);
    return "post success";
  }
}
