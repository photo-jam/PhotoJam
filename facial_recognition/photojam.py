import os
import sys
import shutil
import time
import json
import argparse
from PIL import Image
from subprocess import *

from facenet import load_model, load_data

from align.align_dataset_mtcnn import main as align
import tensorflow as tf
from sklearn.neighbors import KNeighborsClassifier as knn
from sklearn.svm import SVC
import numpy as np

import MySQLdb
import pickle

####################################
# Paths & Globals
####################################

photos_path = "/root/Dropbox/Camera Uploads/"
model_path = "/root/20170512-110547.pb"
new_photos = "/var/www/jamphotos/facial_recognition/new_photos/"
aligned_photos = "/var/www/jamphotos/facial_recognition/aligned_photos/"

default_face_id = 0
default_face_name = "unknown"

###################################
# Database Setup
###################################

try:
    db = MySQLdb.connect(host="photodata.cexbzufp7beq.eu-west-1.rds.amazonaws.com",
                         user=sys.argv[1],
                         passwd=sys.argv[2],
                         db=sys.argv[3])

    cursor = db.cursor()
#    cursor.execute("""DELETE FROM photos""")
#    db.commit()

except:
    print("Invalid Database information. ARGS: jamphotos.py <db_username> <db_pass> <db_name>")

###################################
# Argparser setup for compatibility with facenet align
###################################

parser = argparse.ArgumentParser()
parser.add_argument('--input_dir')
parser.add_argument('--output_dir')
parser.add_argument('--image-size', type=int)
parser.add_argument('--margin', type=int)
parser.add_argument('--random_order')
parser.add_argument('--gpu_memory_fraction', type=float)
parser.add_argument('--has_classes')
parser.add_argument('--no_classes')
align_args = parser.parse_args(['--input_dir', '/var/www/jamphotos/facial_recognition/new_photos/', '--output_dir', '/var/www/jamphotos/facial_recognition/aligned_photos/',
                                '--image-size', '182', '--margin', '44', '--random_order', 'True', '--gpu_memory_fraction', '1.0', '--has_classes', 'False', '--no_classes', 'True'])

##################################
# Get facial embedding for new face
##################################

def get_embedding(new_face_path):
    """
    get_embedding -     function to take in the path of an aligned photo and get
                        the facial embedding from that photo

    args -  new_face_path   path to aligned photo

    returns result      embedding for face
    """
    with tf.Graph().as_default():
        with tf.Session() as sess:

            load_model(model_path)

            # Get input and output tensors
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

            # load photo
            image_paths = [new_face_path]
            images = load_data(image_paths=image_paths, do_random_crop=False,
                               do_random_flip=False, image_size=160, do_prewhiten=True)
            feed_dict = {images_placeholder: images,
                         phase_train_placeholder: False}

            # create an embedding array
            emb_array = sess.run(embeddings, feed_dict=feed_dict)

            return emb_array[0]

#################################
# Predict the newest face
#################################

def predict_face(model, new_embedding):
    print("Predicting face in image...")
    emb_list = [new_embedding]
    predict_response = model.predict_proba(emb_list)
    best_class_indices = np.argmax(predict_response, axis=1)

    return (best_class_indices[0] + 1)

#################################
# Re train model with newest data
#################################

def train_model(data, labels_int):
    clf = SVC(kernel="linear", probability=True)

    print("Training and becoming more self aware...")
    clf.fit(data, labels_int)
    print("New model trained...")

    return clf

#################################
# Parse the database for latest data
#################################

def parse_database():
    data = []
    labels = []
    labels_int = []
    labels_dict = {}
    labels_dict_reverse = {}

    cursor.execute("""SELECT face_id, face_name, embedding FROM photos""")
    result = cursor.fetchall()

    seen_faces = []

    new_faces = []
    new_embeddings = []
    for i in result:
        emb_string = i[2]
        emb = pickle.loads(emb_string)

        # face has been given a new name but still has id of 0
        if i[0] == 0 and i[1] != "unknown":
            # give new face_id
            new_faces.append(i[1])
            new_embeddings.append(emb)

        # face has not yet been given a label, discard data
        elif i[0] == 0 and i[1] == "unknown":
            pass

        #face has been given a new name, update face_id
        elif i[0] != 0 and i[1] in labels_dict_reverse and i[0] != labels_dict_reverse[i[1]]:
            print("HERE1")
            data.append(emb)
            labels_int.append(labels_dict_reverse[i[1]])
            cursor.execute("""UPDATE photos SET face_id=%s WHERE face_name=%s""",
                           (labels_dict_reverse[i[1]], i[1]))
            db.commit()

        # face is known and has a label
        elif i[0] != 0:
            labels_int.append(i[0])
            data.append(emb)
            labels_dict.update({i[0]: i[1]})
            labels_dict_reverse.update({i[1]: i[0]})

        # update label reference dictionary
        if i[1] not in seen_faces and i[0] != 0:
            labels_dict.update({i[0]: i[1]})
            labels_dict_reverse.update({i[1]: i[0]})
            seen_faces.append(i[1])

    # Check for the next available face_id for a new face
    for i in range(0, len(new_faces)):
        # known face, give it the id for that face
        if new_faces[i] in labels_dict.values():
            print("HERE2")
            data.append(new_embeddings[i])
            known_label = labels_dict_reverse[new_faces[i]]
            labels_int.append(labels_dict_reverse[new_faces[i]])
            cursor.execute(
                """UPDATE photos SET face_id=%s WHERE face_name=%s""", (known_label, new_faces[i]))
            db.commit()
            continue

        for x in range(1, 200):
            if x in labels_dict:
                continue
            else:
                print("HERE3")
                cursor.execute(
                    """UPDATE photos SET face_id=%s WHERE face_name=%s""", (x, new_faces[i]))
                db.commit()
                data.append(new_embeddings[i])
                labels_int.append(x)
                labels_dict.update({x: new_faces[i]})
                labels_dict_reverse.update({new_faces[i]: x})
                break

    return data, labels_int, labels_dict

#################################
#Save images as .png
#################################

def save_as_png(file, new_photos_path_jpg, skip):
    if skip:
        return file, new_photos_path_jpg

    #get file_name without extension
    index = file.rfind(".")
    file_name = file[:index]
    file_png = file_name + ".png"
    print("PNG FILE: %s" % file_png)

    #open jpg file, re save it as png and then remove jpg
    jpg_img = Image.open(new_photos_path_jpg)
    new_photos_path_png = os.path.join(new_photos, file_png)
    jpg_img.save(new_photos_path_png)
    print("NEW PNG PATH: %s" % new_photos_path_png)
    os.remove(new_photos_path_jpg)

    return(file_png, new_photos_path_png)

#################################
# Measure Distance
#################################

def compare_dist(a, b):
    dist = np.sqrt(np.sum(np.square(np.subtract(a, b))))
    return dist

#################################
# Update DB
#################################

def update_db(path, file, face_id, face_name, embedding_binary):
    # face_id zero if the label of an unknown face, to be sent to the
    # gui to get a face name
    cursor.execute("""INSERT INTO photos VALUES(%s,%s,%s,%s,%s)""", (
        path, file, face_id, face_name, embedding_binary))
    db.commit()
    print("Database updated...")

#################################
# Main Loop
#################################

def main():
    print("Starting AI backend...")
    print("Waiting for new photo...")
    old_files = []
    try:
        with open('processed_faces', 'rb') as processed_faces:
            old_files = pickle.load(processed_faces)
    except:
        with open('processed_faces', 'wb') as processed_faces:
            pickle.dump(old_files, processed_faces)

    model = None
    # Keep checking for new files into the Dropbox Camera folder
    while(1):
        time.sleep(2)
        new_files = os.listdir(photos_path)
        if new_files != old_files:
            for file in new_files:
                if file not in old_files:
                    print("New photo found...")
                    old_files.append(file)
                    path = os.path.join(photos_path, file)
                    shutil.copy(path, new_photos)
                    new_photos_path_jpg = os.path.join(new_photos, file)

                    #for alignment to work, file may need to be .png
                    file_png, new_photos_path = save_as_png(file, new_photos_path_jpg, True)

                    # Use mtcnn to crop the face
                    align(align_args)

                    # Use FaceNet to extract the embedding of a face
                    print("Extracting embedding of face...")
                    aligned_face_path = os.path.join(aligned_photos, file)
                    aligned_face_path = aligned_face_path.replace(
                        ".jpg", ".png")
                    try:
                        new_face_embedding = get_embedding(aligned_face_path)
                    except:
                        print(
                            "aligning failed...trying to get embedding from raw image")
                        new_face_embedding = get_embedding(path)
                    embedding_binary = pickle.dumps(new_face_embedding)

                    # retrieve the latest data from the database
                    data, labels_int, labels_dict = parse_database()
                    for label in labels_int:
                        print(label, labels_dict[label])

                    #Measure the distances to the other faces
                    #If this is above a threshold for all faces, mark as unknown
                    distances = []

                    #Store distances in a list
                    for i in range(0, len(data)):
                        dist = compare_dist(new_face_embedding, data[i])
                        label = labels_dict[labels_int[i]]
                        print("Distance to %s: %s" % (label, dist))
                        distances.append(dist)

                    #if there is a distance below the threshold, mark as not unknown
                    flag_unknown_face = True
                    early_predict = False
                    early_predict_label_int = None
                    lowest_distance = 2
                    lowest_distance_index = None

                    for i in range(0, len(distances)):
                        if distances[i] < .9 and distances[i] < lowest_distance:
                            lowest_distance_index = i
                            lowest_distance = distances[i]
                            early_predict = True
                            flag_unknown_face = False
                            early_predict_label_int = labels_int[i]
                        elif distances[i] < .9:
                            flag_unknown_face = False
                        else:
                            if distances[i] < lowest_distance:
                                lowest_distance_index = i
                                lowest_distance = distances[i]
                                early_predict_label_int = labels_int[i]

                    #if the check distance algorithm was very sure about a face, label it without classifier
                    if early_predict:
                        print("Distance very close to another photo, performing early prediction...")
                        update_db(path, file, early_predict_label_int, labels_dict[early_predict_label_int], embedding_binary)

                    #add leniancy to the threshold if there is not many images of a new person
                    #check if theres only 1 other image existing of that person
                    elif labels_int.count(labels_int[lowest_distance_index]) <= 2:
                        print("(Threshold increased because this face only appears once before)")
                        print("Distance very close to another photo, performing early prediction...")
                        update_db(path, file, early_predict_label_int, labels_dict[early_predict_label_int], embedding_binary)

                    #if unknown face flag is set, update DB with unknown details
                    elif flag_unknown_face:
                        print("Found a new face...")
                        update_db(path, file, default_face_id, default_face_name, embedding_binary)

                    # not enough data to run training
                    elif len(data) < 3 or len(labels_dict) == 1:
                        print("Tag the first 4 photos before the first model is built.")
                        update_db(path, file, default_face_id, default_face_name, embedding_binary)

                    # run first training
                    elif len(data) >= 3 and model == None and len(labels_dict) > 1:
                        print("Creating first model...")
                        model = train_model(data, labels_int)
                        print(labels_dict)
                        predicted_label_int = predict_face(
                            model, new_face_embedding)
                        print(predicted_label_int)
                        predicted_label_str = labels_dict[predicted_label_int]

                        cursor.execute("""INSERT INTO photos VALUES (%s,%s,%s,%s,%s)""", (
                            path, file, predicted_label_int, predicted_label_str, embedding_binary))
                        db.commit()

                        print("Face information updated...")

                    # predict face and improve model
                    elif len(data) > 3 and model != None:
                        print("Creating a new model...")
                        model = train_model(data, labels_int)
                        predicted_label_int = predict_face(
                            model, new_face_embedding)

                        # TODO if successful prediction
                        if True:
                            predicted_label_str = labels_dict[predicted_label_int]

                            cursor.execute("""INSERT INTO photos VALUES (%s,%s,%s,%s,%s)""", (
                                path, file, predicted_label_int, predicted_label_str, embedding_binary))
                            db.commit()

                            print("Face information updated...")

                    os.remove(new_photos_path)
                    os.remove(aligned_face_path)
                    with open('processed_faces', 'wb') as processed_faces:
                        pickle.dump(old_files, processed_faces)

                        print("Waiting for new photo...")

main()
