<?php

use Illuminate\Http\Request;
use App\Photo;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/photos', 'PhotoApiController@index');

Route::get('/photos/{face_name}', 'PhotoApiController@show_face');

Route::get('/photos/face/{file_name}', 'PhotoApiController@show_image');

Route::post('/photos/{file_name}', 'PhotoApiController@update');

Route::post('/photo_upload', 'PhotoApiController@photo_upload');
